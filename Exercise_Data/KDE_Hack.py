
# coding: utf-8

# In[15]:

import numpy as np
import matplotlib.pyplot as plt

get_ipython().magic('matplotlib inline')

datafile = 'KernelDensity_Data.txt'
data = np.loadtxt(datafile, usecols=(0,1), comments='#')

x = data[:,0]
y = data[:,1]


# In[16]:

def plot_data():
    plt.scatter(x, y, color='k')
    plt.xlim(0,39)
    plt.ylim(0,0.9)
    return


# In[17]:

plot_data()


# In[ ]:



