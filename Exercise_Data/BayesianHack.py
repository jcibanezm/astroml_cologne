import numpy as np
import matplotlib.pyplot as plt

get_ipython().magic('matplotlib inline')

datafile = 'BayesianExercise_data.csv'
data = np.loadtxt(datafile, usecols=(0,1,2), delimiter=',', comments='#')

x = data[:,0]
y = data[:,1]
yerr = data[:,2]

def plot_data():
    plt.errorbar(x, y, yerr=yerr, marker='o', markersize=4, ls='', color='k')
    plt.xlim(0,5)
    plt.ylim(5,10)
    return


# In[8]:

plot_data()


# In[1]:

import emcee


# In[ ]:



