import numpy as np
import os

from sklearn.decomposition import PCA

#SETTINGS:
printStuff = True
numberOfComponents = 10
use_numberOfComponents = False
#SETTINGS end

block1 = False
block2 = True
#INPUT#################################################################
#prepare your own stuff here:
if block1:
    #BLOCK 1
    X = np.random.normal(size=(100, 3))
        #100 points in 3 dimensions

    R = np.random.random((3,10))
        #projection matrix

    X = np.dot(X, R)
        #X is now 10-dim, with 5 intrinsic dims
    #Block 1 end
if block2:
    #Block 2
    X = []
    dataNames = os.listdir("./noisy_data/") #data in this directory is
        #a 12x16 array
    for data in dataNames:
        dataArray = np.loadtxt("./noisy_data/"+data)
        dataArray = dataArray.flatten()
        X.append(dataArray)
    X = np.asarray(X)
    #Block 2 end
#INPUT end#############################################################

#COMPUTING PCA#########################################################
#retuired input:
#   X:
print X.shape
if not use_numberOfComponents: numberOfComponents = min(X.shape)
pca = PCA(n_components = numberOfComponents)
    # n_components can be optionally set
pca.fit(X)
comp = pca.transform(X)
    #compute the subspace projection of X
if printStuff: print 'comp:\n\n', comp, '\n'
mean = pca.mean_
    #length 10 mean of the data
if printStuff: print 'mean:\n\n', mean, '\n'
components = pca.components_
    #matrix of components
if printStuff: print 'components:\n\n', components, '\n'
var = pca.explained_variance_
    #array of eigenvalues
if printStuff: print 'eigenvalues (var):\n\n', var, '\n'
if printStuff: print 'number of components:\n\n', numberOfComponents, '\n'
#COMPUTING PCA end#####################################################

#PLOT##################################################################
for x in range(numberOfComponents):
    total = np.zeros(())
plt.imshow(total.reshape((dataArray.shape))

#PLOT end##############################################################
