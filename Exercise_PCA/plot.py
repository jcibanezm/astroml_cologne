import matplotlib.pyplot as plt
import numpy as np

arr = np.loadtxt("./img.dat")

plt.imshow(arr, cmap="Greys")
plt.show()
