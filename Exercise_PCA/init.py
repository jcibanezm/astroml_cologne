import numpy as np
from scipy import ndimage
import random
import matplotlib.pyplot as plt

numberOnoise = 100

im = ndimage.imread("./initData/Pixel-Mario_laal.jpg", mode = "L")
print im
print im.shape

img_arr = np.zeros((16,12))

def plusminus1():
    return 1 if random.random() < 0.5 else -1

for y in range(16):
    for x in range(12):
        img_arr[y,x] = im[16*y+1, 16*x+1]

np.savetxt("./img.dat", img_arr)

for walk in range(numberOnoise):
    walk_arr = img_arr
    noise_arr = np.random.normal(scale = 1., size = (16,12))
    for x in range(12):
        for y in range(16):

            walk_arr[y,x] = walk_arr[y,x] + noise_arr[y,x] * plusminus1()

    np.savetxt("./noisy_data/noisy_data_"+str(walk)+".dat", noise_arr)
    
    plt.imshow(noise_arr)
    plt.savefig("./noisy_img/noisy_image_"+str(walk)+".png")
    plt.clf()
